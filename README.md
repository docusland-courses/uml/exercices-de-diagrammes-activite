# Mousse au chocolat
Recette de la mousse au chocolat:

- Commencer par casser le chocolat en chocolat en morceaux, puis le faire fondre.
- En parallèle, casser les œufs en séparant les blancs des jaunes.
- Quand le chocolat est fondu, ajouter les jaunes d'oeufs.
- Battre les blancs en neige jusqu'à ce qu'ils soient bien fermes.
- Les incorporer délicatement à la préparation chocolat sans les briser.
- Verser dans des ramequins individuels.
- Mettre au frais au moins 3 heures au réfrigérateur avant de servir.

Représenter par un diagramme d'activités le workflow de la recette de la mousse au chocolat

# Partitions
La situation de cet exercice est celle des formalités d'accueil d'un employé qui vient d'être recruté. Ce scénario commence par l'acceptation du poste par le candidat auprès des ressources humaines. Cette action déclenche simultanément différentes actions auprès de différents services:

- Les ressources humaines préparent les documents d'embauche, puis, soumettent au futur employé le contrat pour signature.
- Le département informatique ouvre un compte pour le nouvel arrivant
- Le secrétariat général se charge d'allouer un bureau au nouvel arrivant.

Proposer un diagramme d'activités partitionné par service.